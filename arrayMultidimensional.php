<!DOCTYPE html>
<!--
 La meva llicencia
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Array Multidimensional</title>
    </head>
    <body>
<?php

/*
 *  La meva llicencia
 */

function arrayMultidimensional($n) {
    
    $array = array();
    
    for ($index = 0; $index < $n; $index++) {
        for ($index1 = 0; $index1 < $n; $index1++) {
            if ($index == $index1) {
                $array[$index][$index1] = "*";
            }else{
                if ($index < $index1) {
                    $array[$index][$index1] = $index + $index1;
                }
                if ($index > $index1){
                    $array[$index][$index1] = rand(10, 20);
                }
            }
        }
    }
    
    mostrarTaula($array);

    $array2 = intercanviarValors($array);
    
    mostrarTaula($array2);
}

function mostrarTaula ($array) {
    echo "<table border='1px solid black'>";
    for ($index = 0; $index < count($array); $index++) {
        echo "<tr>";
        for ($index1 = 0; $index1 < count($array); $index1++) {
            echo "<td>" . $array[$index][$index1] . "</td>";
        }
        echo "</tr>";
    }
    echo "<table><br/>";
}

function intercanviarValors($array){
    $array2 = array();
    for ($index = 0; $index < count($array); $index++) {        
        for ($index1 = 0; $index1 < count($array); $index1++) {
            $array2[$index][$index1] = $array[$index1][$index];
        }
    }
    return $array2;
}

arrayMultidimensional(5);
?>
    </body>
</html>