<!DOCTYPE html>
<!--
 La meva llicencia
To change this template file, choose Tools | Templates
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Factorial Array</title>
    </head>
    <body>
        <?php
        
            $arrayNombres = array(1,5,100);
        
            function factorialArray($arrayNombres){
                $arrayFinal = "";
                if (is_array($arrayNombres)){
                    for ($index = 0; $index < count($arrayNombres); $index++) {
                        if (!is_int($arrayNombres[$index])){
                            echo "<p>false</p>";
                            return false;
                        }
                        $arrayFinal[$index] = recursiuArray($arrayNombres[$index]);
                        echo "<p>" . $arrayFinal[$index] . "</p>";
                    }
                    
                }else{
                    echo "<p>false</p>";
                    return false;
                }
            }
            
            function recursiuArray($n){
                if($n == 0){
                   return 1; 
                }
                   return $n*recursiuArray($n-1);
            }
            
            factorialArray($arrayNombres);
            
        ?>
    </body>
</html>
